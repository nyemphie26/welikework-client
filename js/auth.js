class Auth {
    constructor(){
        document.querySelector("body").style.display = "none";
        const auth = localStorage.getItem("auth");
        this.validateAuth(auth);

    }

    validateAuth(auth){
        if(auth == null){
            window.location.replace("/pages/login.html");
        }
        else{
            document.querySelector("body").style.display = "block";
        }
    }

    logOut(){
        fetch("https://welikeworkrequests.ca/api/logout",{
                    method: "POST",
                    headers: new Headers({
                        'Authorization': 'Bearer '+localStorage.getItem("auth"), 
                        'Content-Type': 'application/json'
                      })
                })
                    .then((response) => response.json())
                    .then((data)=>{
                        console.log(data);
                        if (data.message) {
                            db.delete()
                            localStorage.removeItem("auth");
                            window.location.replace("/");
                        } 
                        else {
                        }
                    })
    }
}