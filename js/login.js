class login {
    constructor(form, fields){
        this.form = form;
        this.fields = fields;
        const auth = localStorage.getItem("auth");
        this.validateAuth(auth);
        this.validateonSubmit();
    }

    validateAuth(auth){
        if(auth != null){
            window.location.replace("/");
        }
        else{
        }
    }
    validateonSubmit(){
        let self = this;

        this.form.addEventListener("submit", (e) => {
            e.preventDefault();
            var error = 0;
            self.fields.forEach((field)=>{
                const input = document.querySelector(`#${field}`);
                if (self.validateFields(input) == false) {
                    error++;
                } 
            });
            if (error == 0) {
                //login api
                var data = {
                    email: document.querySelector(`#email`).value,
                    password: document.querySelector(`#password`).value,
                };

                fetch("https://welikeworkrequests.ca/api/login",{
                    method: "POST",
                    body: JSON.stringify(data),
                    headers:{
                        'Content-Type': 'application/json',
                    },
                })
                    .then((response) => response.json())
                    .then((data)=>{
                        console.log(data);
                        if (data.token) {
                            localStorage.setItem("auth",data.token);
                            this.form.submit();
                        } 
                        else {
                            this.setStatus(document.querySelector(".input"),data.message,"error");    
                        }
                    })
            }
        });
    }

    validateFields(field){
        if(field.value.trim() == "") {
            this.setStatus(
                field,
                `${field.previousElementSibling.id} cannot be blank`,
                "error"
            );
            return false;
        }
        else{
            this.setStatus(field,null,"success");
        }
    }

    setStatus(field,message,status){
        const errorMessage = field.parentElement.querySelector(".error-message");

        if (status == "success") {
            if(errorMessage){
                errorMessage.innerText = "";
            }
            field.classList.remove("input-error");
        }

        if(status == "error"){
            errorMessage.innerText = message;
            field.classList.add("input-error");
        }

    }
}

const form = document.querySelector(".loginForm");

if (form) {
    const fields = ["email","password"];
    const validator = new login(form,fields);
} else {
    
}