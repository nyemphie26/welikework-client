refreshData();

let btnAdd = document.getElementById('refresh');

btnAdd.addEventListener('click', saveDataToDb)


function getWorksFromAPI(){
const data = fetch('https://welikeworkrequests.ca/api/works', { 
                  method: 'get', 
                  headers: new Headers({
                    'Authorization': 'Bearer '+localStorage.getItem("auth"), 
                    'Content-Type': 'application/json'
                  })
                });
    return data;
}

function saveDataToDb (){
  db.collection('works').delete();
  console.log(localStorage.getItem("auth"))
  getWorksFromAPI()
  .then(response => response.json())
  .then(responseJSON => {
    for(const key in responseJSON.data) {
      db.collection('works').add({
        id: responseJSON.data[key].id,
        service: responseJSON.data[key].work.service.service_name,
        cust: responseJSON.data[key].work.cust.name+' '+responseJSON.data[key].work.cust.last_name,
        phone: responseJSON.data[key].work.cust.phone_number,
        pickUp: responseJSON.data[key].work.location,
        dropOff: responseJSON.data[key].work.drop_off_location,
        custNotes: responseJSON.data[key].work.cust_notes,
        knownPref: responseJSON.data[key].work.known_pref,
        jobDesc: responseJSON.data[key].work.job_desc,
        workStatus: responseJSON.data[key].work.status,
        schedule: responseJSON.data[key].work.schedule.sch_date,
        fees: responseJSON.data[key].work.billing
      })
      .then(()=> {
            console.log('Work been added')
          })
        }
      })
  .then(()=>{
    refreshData()
  })
}

function refreshData(){
  db.collection('works').get().then(works => {
    clearData();
    for(const key in works) {
      renderData(works[key],works[key].id)
    }
  })
}

function renderModal(id){
  
}

const worksCard = document.querySelector('.works');
var elems = document.querySelectorAll('.modal');

worksCard.addEventListener('click', evt => {
  // console.log(evt.target.getAttribute('data-id'))
  const id = parseInt(evt.target.getAttribute('data-id'));
  var cust = document.getElementById('cust')
      phone = document.getElementById('phone')
      pickUpLoc = document.getElementById('pickUpLoc')
      dropOffLoc = document.getElementById('dropOffLoc')
      custNotes = document.getElementById('custNotes')
      knownPref = document.getElementById('knownPref')
      jobDesc = document.getElementById('jobDesc')
      fees = document.getElementById('fees')
      head = document.getElementById('header')
      instances = M.Modal.init(elems)

  db.collection('works').doc({ id: id }).get().then(document => {
    M.updateTextFields()
    date = new Date(document.schedule)
    head.innerText = document.service+' -- '+date.toLocaleDateString("en-CA",{year: 'numeric', month: 'short', day: 'numeric'})+ ' ('+date.toLocaleString("en-CA",{hour: 'numeric', minute: 'numeric'})+')'
    cust.value = document.cust
    phone.value = document.phone
    pickUpLoc.value = document.pickUp.address+' '+document.pickUp.town+' '+document.pickUp.province+' ('+document.pickUp.postal+')'
    if (document.dropOff) {
      dropOffLoc.value = document.drop_off_location.address+' '+document.drop_off_location.town+' '+document.drop_off_location.province+' ('+document.drop_off_location.postal+')'
    }
    custNotes.value = document.custNotes
    knownPref.value = document.knownPref
    jobDesc.value = document.jobDesc
    fees.value = '$'+document.fees.total_fees
  })
  // console.log(a.id);
})



  