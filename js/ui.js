const worksPanel = document.querySelector('.works');

document.addEventListener('DOMContentLoaded', function() {
  // nav menu
  const menus = document.querySelectorAll('.side-menu');
  M.Sidenav.init(menus, {edge: 'right'});

  // details form
  // var elems = document.querySelectorAll('.modal');
  // var instances = M.Modal.init(elems);
  
});



//render data

const renderData = (data,id) => {

  const html =`
    <div class="card-panel work white row"  data-id="${id}">
      <img src="/img/icons/icon-96x96.png" alt="work thumb" data-id="${id}">
      <a class="modal-trigger" href="#details-modal" data-id="${id}">
        <div class="work-details">
          <div class="work-title" data-id="${id}">${data.service}</div>
          <div class="work-ingredients" data-id="${id}">${data.schedule}</div>
        </div>
      </a>
    </div>
  `;

  worksPanel.innerHTML += html
};

const clearData = () => {
  const html = ``;

  worksPanel.innerHTML = html;
};

// const renderModal = (id) => {
 
// };

